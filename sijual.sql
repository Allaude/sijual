-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12 Agu 2017 pada 14.54
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sijual`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cash`
--

CREATE TABLE `cash` (
  `id_cash` int(20) NOT NULL,
  `cash` int(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cash`
--

INSERT INTO `cash` (`id_cash`, `cash`, `keterangan`, `date`, `id_users`) VALUES
(1, 3000000, 'tes', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `diskon`
--

CREATE TABLE `diskon` (
  `id_diskon` int(20) NOT NULL,
  `diskon` int(50) NOT NULL,
  `keterangan` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `diskon`
--

INSERT INTO `diskon` (`id_diskon`, `diskon`, `keterangan`, `date`, `id_users`) VALUES
(1, 10000, 'tes', '2017-08-12', 2),
(2, 30000, 'tesss', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `edc_bca`
--

CREATE TABLE `edc_bca` (
  `id_edc_bca` int(50) NOT NULL,
  `edc_bca` int(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `edc_bca`
--

INSERT INTO `edc_bca` (`id_edc_bca`, `edc_bca`, `keterangan`, `date`, `id_users`) VALUES
(1, 5000000, 'tes', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `edc_bni`
--

CREATE TABLE `edc_bni` (
  `id_edc_bni` int(20) NOT NULL,
  `edc_bni` int(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `edc_bni`
--

INSERT INTO `edc_bni` (`id_edc_bni`, `edc_bni`, `keterangan`, `date`, `id_users`) VALUES
(1, 3000000, 'tesss', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hybrid`
--

CREATE TABLE `hybrid` (
  `id_hybrid` int(20) NOT NULL,
  `hybrid` int(30) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hybrid`
--

INSERT INTO `hybrid` (`id_hybrid`, `hybrid`, `keterangan`, `date`, `id_users`) VALUES
(1, 2000000, 'tes', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `modal`
--

CREATE TABLE `modal` (
  `id_modal` int(20) NOT NULL,
  `modal` int(20) NOT NULL,
  `keterangan` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `modal`
--

INSERT INTO `modal` (`id_modal`, `modal`, `keterangan`, `date`, `id_users`) VALUES
(3, 2000000, '', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `operasional`
--

CREATE TABLE `operasional` (
  `id_operasional` int(20) NOT NULL,
  `operasional` int(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `operasional`
--

INSERT INTO `operasional` (`id_operasional`, `operasional`, `keterangan`, `date`, `id_users`) VALUES
(1, 5000, 'tes', '2017-08-12', 2),
(2, 10000, 'tessss', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelunasan`
--

CREATE TABLE `pelunasan` (
  `id_pelunasan` int(20) NOT NULL,
  `pelunasan` int(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelunasan`
--

INSERT INTO `pelunasan` (`id_pelunasan`, `pelunasan`, `keterangan`, `date`, `id_users`) VALUES
(1, 5000, 'tesss', '2017-08-12', 2),
(2, 1000, 'tes2', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `piutang`
--

CREATE TABLE `piutang` (
  `id_piutang` int(20) NOT NULL,
  `piutang` int(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `piutang`
--

INSERT INTO `piutang` (`id_piutang`, `piutang`, `keterangan`, `date`, `id_users`) VALUES
(1, 5000, 'tes', '2017-08-12', 2),
(2, 2000, 'tes2', '2017-08-12', 2),
(3, 1000, 'tes3', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_keluar`
--

CREATE TABLE `uang_keluar` (
  `id_uang_keluar` int(20) NOT NULL,
  `uang_keluar` int(20) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `uang_keluar`
--

INSERT INTO `uang_keluar` (`id_uang_keluar`, `uang_keluar`, `keterangan`, `date`, `id_users`) VALUES
(1, 1000, 'tes', '2017-08-12', 2),
(2, 2000, 'tess', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_masuk`
--

CREATE TABLE `uang_masuk` (
  `id_uang_masuk` int(20) NOT NULL,
  `uang_masuk` int(20) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `id_users` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `uang_masuk`
--

INSERT INTO `uang_masuk` (`id_uang_masuk`, `uang_masuk`, `keterangan`, `date`, `id_users`) VALUES
(1, 50000, 'kas', '2017-08-12', 2),
(2, 100000, 'kas lagi', '2017-08-12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `user`, `password`, `level`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1),
(2, 'arfi', 'd062c0e7a3c648f00bbdd60683f89c2ce57801b6', 2),
(3, 'tesha', 'cf679132d6b16f03737cdd8105ddac139b22b55b', 2),
(8, 'wahyu', '3b7375a688b1820b016224646365e127de125ff0', 2),
(9, 'alfi', '729a21ab84bc7ec170d34bc1ffa07de039927336', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cash`
--
ALTER TABLE `cash`
  ADD PRIMARY KEY (`id_cash`);

--
-- Indexes for table `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`id_diskon`);

--
-- Indexes for table `edc_bca`
--
ALTER TABLE `edc_bca`
  ADD PRIMARY KEY (`id_edc_bca`);

--
-- Indexes for table `edc_bni`
--
ALTER TABLE `edc_bni`
  ADD PRIMARY KEY (`id_edc_bni`);

--
-- Indexes for table `hybrid`
--
ALTER TABLE `hybrid`
  ADD PRIMARY KEY (`id_hybrid`);

--
-- Indexes for table `modal`
--
ALTER TABLE `modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `operasional`
--
ALTER TABLE `operasional`
  ADD PRIMARY KEY (`id_operasional`);

--
-- Indexes for table `pelunasan`
--
ALTER TABLE `pelunasan`
  ADD PRIMARY KEY (`id_pelunasan`);

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`id_piutang`);

--
-- Indexes for table `uang_keluar`
--
ALTER TABLE `uang_keluar`
  ADD PRIMARY KEY (`id_uang_keluar`);

--
-- Indexes for table `uang_masuk`
--
ALTER TABLE `uang_masuk`
  ADD PRIMARY KEY (`id_uang_masuk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cash`
--
ALTER TABLE `cash`
  MODIFY `id_cash` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `diskon`
--
ALTER TABLE `diskon`
  MODIFY `id_diskon` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `edc_bca`
--
ALTER TABLE `edc_bca`
  MODIFY `id_edc_bca` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `edc_bni`
--
ALTER TABLE `edc_bni`
  MODIFY `id_edc_bni` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hybrid`
--
ALTER TABLE `hybrid`
  MODIFY `id_hybrid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modal`
--
ALTER TABLE `modal`
  MODIFY `id_modal` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `operasional`
--
ALTER TABLE `operasional`
  MODIFY `id_operasional` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pelunasan`
--
ALTER TABLE `pelunasan`
  MODIFY `id_pelunasan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `id_piutang` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `uang_keluar`
--
ALTER TABLE `uang_keluar`
  MODIFY `id_uang_keluar` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uang_masuk`
--
ALTER TABLE `uang_masuk`
  MODIFY `id_uang_masuk` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
