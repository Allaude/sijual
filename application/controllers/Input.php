<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Input extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('inputs','users'));
		if($this->users->logged_in() == FALSE){
			redirect('auth','refresh');
		}		
	}

	public function index()
	{
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$data['modal'] = $this->inputs->modal_check($user,$date);
		$data['hybrid'] = $this->inputs->hybrid_check($user,$date);
		$data['cash'] = $this->inputs->cash_check($user,$date);
		$data['edc_bca'] = $this->inputs->edc_bca_check($user,$date);
		$data['edc_bni'] = $this->inputs->edc_bni_check($user,$date);
		$data['diskon'] = $this->inputs->diskon_check($user,$date);
		$data['uang_keluar'] = $this->inputs->uang_keluar_check($user,$date);
		$data['pelunasan'] = $this->inputs->pelunasan_check($user,$date);
		$data['operasional'] = $this->inputs->operasional_check($user,$date);
		$data['piutang'] = $this->inputs->piutang_check($user,$date);
		$data['uang_masuk'] = $this->inputs->uang_masuk_check($user,$date);
		$this->load->view('template/header');
		$this->load->view('input/view',$data);
		$this->load->view('template/footer');
	}
	//modal segment
	public function modal_insert(){
		$data = array(
			'modal'			=>	$this->input->post('modal'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->modal_insert($data);
		echo json_encode($result);
	}
	public function modal_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->modal_check($user,$date);
		echo json_encode($result);
	}
	public function modal_update($id){
		$data = array(
			'modal'			=>	$this->input->post('modal'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->modal_update($id,$data);
		echo json_encode($result);
	}
	//hybrid segment
	public function hybrid_insert(){
		$data = array(
			'hybrid'		=>	$this->input->post('hybrid'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->hybrid_insert($data);
		echo json_encode($result);
	}
	public function hybrid_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->hybrid_check($user,$date);
		echo json_encode($result);
	}
	public function hybrid_update($id){
		$data = array(
			'hybrid'		=>	$this->input->post('hybrid'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->hybrid_update($id,$data);
		echo json_encode($result);
	}
	//cash segment
	public function cash_insert(){
		$data = array(
			'cash'			=>	$this->input->post('cash'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->cash_insert($data);
		echo json_encode($result);
	}
	public function cash_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->cash_check($user,$date);
		echo json_encode($result);
	}
	public function cash_update($id){
		$data = array(
			'cash'			=>	$this->input->post('cash'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->cash_update($id,$data);
		echo json_encode($result);
	}
	//edc_bca segment
	public function edc_bca_insert(){
		$data = array(
			'edc_bca'		=>	$this->input->post('edc_bca'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->edc_bca_insert($data);
		echo json_encode($result);
	}
	public function edc_bca_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->edc_bca_check($user,$date);
		echo json_encode($result);
	}
	public function edc_bca_update($id){
		$data = array(
			'edc_bca'		=>	$this->input->post('edc_bca'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->edc_bca_update($id,$data);
		echo json_encode($result);
	}
	//edc bni segment
	public function edc_bni_insert(){
		$data = array(
			'edc_bni'		=>	$this->input->post('edc_bni'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->edc_bni_insert($data);
		echo json_encode($result);
	}
	public function edc_bni_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->edc_bni_check($user,$date);
		echo json_encode($result);
	}
	public function edc_bni_update($id){
		$data = array(
			'edc_bni'		=>	$this->input->post('edc_bni'),
			'keterangan'	=>	$this->input->post('ket'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->edc_bni_update($id,$data);
		echo json_encode($result);
	}
	//diskon segment
	public function diskon_insert(){
		$data = array(
			'diskon'		=>	$this->input->post('diskon'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->diskon_insert($data);
		echo json_encode($result);
	}
	public function diskon_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->diskon_check($user,$date);
		echo json_encode($result);
	}
	public function diskon_update($id){
		$data = array(
			'diskon'		=>	$this->input->post('diskon'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->diskon_update($id,$data);
		echo json_encode($result);
	}
	//uang_keluar segment
	public function uang_keluar_insert(){
		$data = array(
			'uang_keluar'	=>	$this->input->post('uang_keluar'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->uang_keluar_insert($data);
		echo json_encode($result);
	}
	public function uang_keluar_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->uang_keluar_check($user,$date);
		echo json_encode($result);
	}
	public function uang_keluar_update($id){
		$data = array(
			'uang_keluar'	=>	$this->input->post('uang_keluar'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->uang_keluar_update($id,$data);
		echo json_encode($result);
	}
	//pelunasan segment
	public function pelunasan_insert(){
		$data = array(
			'pelunasan'		=>	$this->input->post('pelunasan'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->pelunasan_insert($data);
		echo json_encode($result);
	}
	public function pelunasan_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->pelunasan_check($user,$date);
		echo json_encode($result);
	}
	public function pelunasan_update($id){
		$data = array(
			'pelunasan'		=>	$this->input->post('pelunasan'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->pelunasan_update($id,$data);
		echo json_encode($result);
	}
	//operasional segment
	public function operasional_insert(){
		$data = array(
			'operasional'	=>	$this->input->post('operasional'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->operasional_insert($data);
		echo json_encode($result);
	}
	public function operasional_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->operasional_check($user,$date);
		echo json_encode($result);
	}
	public function operasional_update($id){
		$data = array(
			'operasional'	=>	$this->input->post('operasional'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->operasional_update($id,$data);
		echo json_encode($result);
	}
	//piutang segment
	public function piutang_insert(){
		$data = array(
			'piutang'		=>	$this->input->post('piutang'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->piutang_insert($data);
		echo json_encode($result);
	}
	public function piutang_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->piutang_check($user,$date);
		echo json_encode($result);
	}
	public function piutang_update($id){
		$data = array(
			'piutang'		=>	$this->input->post('piutang'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->piutang_update($id,$data);
		echo json_encode($result);
	}
	//uang_masuk segment
	public function uang_masuk_insert(){
		$data = array(
			'uang_masuk'	=>	$this->input->post('uang_masuk'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->uang_masuk_insert($data);
		echo json_encode($result);
	}
	public function uang_masuk_check(){
		$user = $this->session->userdata('id_user');
		$date = date("Y-m-d");
		$result = $this->inputs->uang_masuk_check($user,$date);
		echo json_encode($result);
	}
	public function uang_masuk_update($id){
		$data = array(
			'uang_masuk'	=>	$this->input->post('uang_masuk'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'date'			=>	date("Y-m-d"),
			'id_users'		=>	$this->session->userdata('id_user'));
		$result = $this->inputs->uang_masuk_update($id,$data);
		echo json_encode($result);
	}
	/*
	function chart(){
		$var = array();
		$db = $this->inputs->date_counter();
		foreach ($db->result_array() as $key) {
			$diskon = $this->inputs->sum_date($key['date']);
			$v_diskon = 0;
			foreach ($diskon->result_array() as $d) {
				$v_diskon += $d['jumlah'];
			}
			$array = array(
				'value'		=>	$v_diskon,
				'date'		=>	$key['date']);
			array_push($var, $array);
		}
		$footer['chart'] = json_encode($var);
		$footer['value'] = 'value';
		$this->load->view('template/header');
		$this->load->view('pemasukan/chart');
		$this->load->view('template/footer',$footer);
	}
	*/
}

/* End of file Pemasukan.php */
/* Location: ./application/controllers/Pemasukan.php */