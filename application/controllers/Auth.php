<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->library('form_validation');
		$this->load->helper(array('url','html'));
	}

	public function index()
	{
		redirect('auth/login');
	}
	public function login(){
		//set error delimiter to showing into views
		$this->form_validation->set_error_delimiters('','<br />');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[2]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[2]');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login');
		} else {
			$username = $this->input->post('username');
			$password = sha1($this->input->post('password'));
			$valid = $this->users->check_user($username,$password);
			if($valid == 'NO_USER'){
				$this->session->set_flashdata('message', 'PLEASE CHECK USERNAME');
				$this->load->view('login');
			} else if($valid == 'PASSWORD_FALSE'){
				$this->session->set_flashdata('message', 'PLEASE CHECK PASSWORD');
				$this->load->view('login');
			} else {
				if($valid['user'] === 'admin'){
				$data = array(
					'loged'		=>	TRUE,
					'user'		=>	$username,
					'id_user'	=>	$valid['id_user']);
				$this->session->set_userdata($data);
				redirect('user','refresh');					
				}
				$data = array(
					'loged'		=>	TRUE,
					'user'		=>	$username,
					'id_user'	=>	$valid['id_user']);
				$this->session->set_userdata($data);
				redirect('input','refresh');
			}
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		$this->load->view('login');
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */