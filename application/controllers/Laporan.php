<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('users','inputs'));
		if($this->users->logged_in() == FALSE){
			redirect('auth','refresh');
		}
		if($this->session->userdata('id_user') !== '1'){
			redirect('input','refresh');
		}
	}

	public function index()
	{
		$data['user'] = $this->users->get_all_karyawan();
		$this->load->view('template/header');
		$this->load->view('laporan/view',$data);
		$this->load->view('template/footer');
	}
	public function all(){
	}
	public function one(){
		$date = $this->input->post('date_laporan');
		$user = $this->input->post('user_laporan');
		$data['modal'] = $this->inputs->modal_check($user,$date);
		$data['hybrid'] = $this->inputs->hybrid_check($user,$date);
		$data['cash'] = $this->inputs->cash_check($user,$date);
		$data['edc_bca'] = $this->inputs->edc_bca_check($user,$date);
		$data['edc_bni'] = $this->inputs->edc_bni_check($user,$date);
		$data['diskon'] = $this->inputs->diskon_check($user,$date);
		$data['uang_keluar'] = $this->inputs->uang_keluar_check($user,$date);
		$data['pelunasan'] = $this->inputs->pelunasan_check($user,$date);
		$data['operasional'] = $this->inputs->operasional_check($user,$date);
		$data['piutang'] = $this->inputs->piutang_check($user,$date);
		$data['uang_masuk'] = $this->inputs->uang_masuk_check($user,$date);
		$this->load->view('laporan/cetak',$data);		
	}

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */