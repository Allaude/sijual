<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		if($this->users->logged_in() == FALSE){
			redirect('auth','refresh');
		}
		if($this->session->userdata('id_user') !== '1'){
			redirect('input','refresh');
		}
	}

	public function index()
	{
		$data['user'] = $this->users->get_all_user();
		$this->load->view('template/header');
		$this->load->view('user/view',$data);
		$this->load->view('template/footer');
	}
	public function input(){
		$data = array(
			'user'		=>	$this->input->post('username'),
			'password'	=> sha1($this->input->post('password')),
			'level'		=>	$this->input->post('level'));
		$result = $this->users->insert($data);
		echo json_encode($result);
	}
	public function update($id){
		$data = array(
			'user'		=>	$this->input->post('username'),
			'password'	=> sha1($this->input->post('password')),
			'level'		=>	$this->input->post('level'));
		$result = $this->users->update($id,$data);
		echo json_encode($result);		
	}
	public function get_detail_user($id){
		$data = $this->users->get_data_by_id($id)->row_array();
		echo json_encode($data);
	}
	public function delete($id){
		$result = $this->users->delete($id);
		echo json_encode($result);
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */