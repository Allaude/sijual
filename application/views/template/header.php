<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sijual</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bootstrap/css//morris.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="<?php echo base_url(); ?>" class="logo">
      <span class="logo-lg"><b>JNE </b>COUNTER</span>
    </a>
    <nav class="navbar navbar-static-top">
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#">Hello <?php echo $this->session->userdata('user'); ?></a></li>
          <li><?php echo anchor('auth/logout', 'Logout'); ?></li>
        </ul>
      </div>
    </nav>
  </header>
        <?php if($this->session->userdata('user') === 'admin'){ ?>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN UTAMA</li>
        <?php $page = $this->uri->segment(1);
        switch ($page) {
          case 'laporan':
              $laporan ='active';
              break;
          case 'user':
              $user = 'active';
            break;
         } ?>
          <li class="<?php (isset($user) ? $user = $user : $user = ''); echo $user; ?>"><?php echo anchor('user','<span>User</span>'); ?></li>
          <li class="<?php (isset($laporan) ? $laporan = $laporan : $laporan = ''); echo $laporan; ?>"><?php echo anchor('laporan','<span>Laporan</span>'); ?></li>
      </ul>
    </section>
  </aside>
        <?php } ?>