<div class="content-wrapper" id="main">
  <div class="container">
    <section class="content-header">
      <h1>Menu <?php echo ucfirst($this->uri->segment(1)); ?></h1><br>
    </section>
    <section id="loading_user">
      <div class="row">
        <div class="col-lg-7 col-lg-offset-5 col-xs-7 col-xs-offset-5 col-md-7 col-md-offset-5 ">
          <img align="center" alt="loading" src="<?php echo base_url(); ?>assets/img/gear.svg">
        </div>
      </div>
    </section>
    <section id="main_user">
      <button class="btn btn-success" id="add_user">Add User</button>
      <table class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>User</th>
            <th>Password</th>
            <th>Level</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($user->result_array() as $u) { ?>
          <tr>
            <td><?= $u['user'] ?></td>
            <td><?= $u['password'] ?></td>
            <td><?= ($u['level'] == 1) ? $level = 'administrator' : $level = 'karyawan'; $level; ?></td>
            <td>
              <button class="btn btn-info" id="update_user" value="<?= $u['id_user'] ?>">Update</button>
              <button class="btn btn-danger" id="delete_user" value="<?= $u['id_user'] ?>">Delete</button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </section>
    <section id="form_user">
        <form class="form-horizontal">
          <input type="hidden" id="id_user" value="">
          <div class="control-group">
            <label class="control-label">User</label>
            <div class="controls">
              <input type="text" id="inputUser" placeholder="User" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
              <input type="password" id="inputPassword" placeholder="Password" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Level</label>
            <div class="controls">
              <select class="form-control" id="inputLevel">
                <option value="1">Administrator</option>
                <option value="2">Karyawan</option>
              </select>
            </div>
          </div>
          <br>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-info" id="submit_user">Submit</button>
              <button type="cancel" class="btn btn-danger" id="cancel_input_user">Cancel</button>
            </div>
          </div>
        </form>
    </section>        
  </div>
</div>