<div style="background-color: #ecf0f1">
  <div class="container">
    <section class="content-header">
      <h1>Menu <?php echo ucfirst($this->uri->segment(1)); ?></h1><br>
    </section>
    <section id="loading_input">
      <div class="row">
        <div class="col-lg-7 col-lg-offset-5 col-xs-7 col-xs-offset-5 col-md-7 col-md-offset-5 ">
          <img align="center" alt="loading" src="<?php echo base_url(); ?>assets/img/gear.svg">
        </div>
      </div>
    </section>
    <section id="main_user">
      <button class="btn btn-success" id="add_input_modal">Modal</button>
      <button class="btn btn-success" id="add_input_hybrid">Hybrid</button>
      <button class="btn btn-success" id="add_input_cash">Cash</button>
      <button class="btn btn-success" id="add_input_edc_bca">EDC BCA</button>
      <button class="btn btn-success" id="add_input_edc_bni">EDC BNI</button>
      <button class="btn btn-success" id="add_input_diskon">Diskon</button>
      <button class="btn btn-success" id="add_input_uang_keluar">Uang Keluar</button>
      <button class="btn btn-success" id="add_input_pelunasan">Pelunasan</button>
      <button class="btn btn-success" id="add_input_operasional">Operasional</button>
      <button class="btn btn-success" id="add_input_piutang">Piutang</button>
      <button class="btn btn-success" id="add_input_uang_masuk">Uang Masuk</button>
      <table class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>Modal</th>
            <th>Hybrid</th>
            <th>Cash</th>
            <th>EDC BCA</th>
            <th>EDC BNI</th>
            <th>Diskon</th>
            <th>Uang Keluar</th>
            <th>Pelunasan</th>
            <th>Operasional</th>
            <th>Piutang</th>
            <th>Uang Masuk</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td><?php echo $modal['modal']; ?></td>
          <td><?php echo $hybrid['hybrid']; ?></td>
          <td><?php echo $cash['cash']; ?></td>
          <td><?php echo $edc_bca['edc_bca']; ?></td>
          <td><?php echo $edc_bca['edc_bca']; ?></td>
          <td>
            <?php if($diskon != FALSE){ foreach ($diskon as $d) {
              echo $d['diskon']."<br>";
            }} ?>
          </td>
          <td>
            <?php if($uang_keluar != FALSE){ foreach ($uang_keluar as $uk) {
              echo $uk['uang_keluar']."<br>";
            }} ?>
          </td>
          <td>
            <?php if($pelunasan != FALSE){ foreach ($pelunasan as $pl) {
              echo $pl['pelunasan']."<br>";
            }} ?>
          </td>
          <td>
            <?php if($operasional != FALSE){ foreach ($operasional as $o) {
              echo $o['operasional']."<br>";
            }} ?>
          </td>
          <td>
            <?php if($piutang != FALSE){ foreach ($piutang as $p) {
              echo $p['piutang']."<br>";
            }} ?>
          </td>
          <td>
            <?php if($uang_masuk != FALSE){ foreach ($uang_masuk as $um) {
              echo $um['uang_masuk']."<br>";
            }} ?>
          </td>
        </tr>
        </tbody>
      </table>
    </section>
    <section id="form_input_modal">
        <form class="form-horizontal">
          <input type="hidden" id="id_input_modal" value="">
          <div class="control-group">
            <label class="control-label">Modal</label>
            <div class="controls">
              <input type="text" id="input_modal" placeholder="Modal" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
              <input type="text" id="input_modal_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">
            </div>
          </div>
          <br>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-info" id="submit_input_modal">Submit</button>
              <button type="cancel" class="btn btn-danger" id="cancel_input_modal">Cancel</button>
            </div>
          </div>
        </form>
    </section>
    <section id="form_input_hybrid">
        <form class="form-horizontal">
          <input type="hidden" id="id_input_hybrid" value="">
          <div class="control-group">
            <label class="control-label">Hybrid</label>
            <div class="controls">
              <input type="text" id="input_hybrid" placeholder="Hybrid" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
              <input type="text" id="input_hybrid_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">
            </div>
          </div>
          <br>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-info" id="submit_input_hybrid">Submit</button>
              <button type="cancel" class="btn btn-danger" id="cancel_input_hybrid">Cancel</button>
            </div>
          </div>
        </form>
    </section>
    <section id="form_input_cash">
        <form class="form-horizontal">
          <input type="hidden" id="id_input_cash" value="">
          <div class="control-group">
            <label class="control-label">Cash</label>
            <div class="controls">
              <input type="text" id="input_cash" placeholder="cash" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
              <input type="text" id="input_cash_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">
            </div>
          </div>
          <br>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-info" id="submit_input_cash">Submit</button>
              <button type="cancel" class="btn btn-danger" id="cancel_input_cash">Cancel</button>
            </div>
          </div>
        </form>
    </section>
    <section id="form_input_edc_bca">
        <form class="form-horizontal">
          <input type="hidden" id="id_input_edc_bca" value="">
          <div class="control-group">
            <label class="control-label">edc bca</label>
            <div class="controls">
              <input type="text" id="input_edc_bca" placeholder="edc bca" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
              <input type="text" id="input_edc_bca_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">
            </div>
          </div>
          <br>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-info" id="submit_input_edc_bca">Submit</button>
              <button type="cancel" class="btn btn-danger" id="cancel_input_edc_bca">Cancel</button>
            </div>
          </div>
        </form>
    </section>
    <section id="form_input_edc_bni">
        <form class="form-horizontal">
          <input type="hidden" id="id_input_edc_bni" value="">
          <div class="control-group">
            <label class="control-label">edc bni</label>
            <div class="controls">
              <input type="text" id="input_edc_bni" placeholder="edc bni" class="form-control">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
              <input type="text" id="input_edc_bni_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">
            </div>
          </div>
          <br>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-info" id="submit_input_edc_bni">Submit</button>
              <button type="cancel" class="btn btn-danger" id="cancel_input_edc_bni">Cancel</button>
            </div>
          </div>
        </form>
    </section>
    <section id="form_input_diskon">
        <form class="form-horizontal">
          <h5>Harap Save terlebih dahulu pekerjaan anda sebelum tekan tombol Back</h5>
          <button type="cancel" class="btn btn-danger" id="cancel_input_diskon">Back</button>
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>Diskon</th>
                <th>Keterangan</th>
                <th><button type="submit" class="btn btn-success" id="add_row_input_diskon">Add Diskon</button></th>
              </tr>
            </thead>
            <tbody id="table_input_diskon">
              <tr>
                <input type="hidden" id="id_input_diskon" value="">
                <td>
                  <input type="text" id="input_diskon" placeholder="diskon" class="form-control">
                </td>
                <td>
                  <input type="text" id="input_diskon_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">                  
                </td>
                <td>
                  <button type="submit" class="btn btn-info" id="submit_input_diskon">Save</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    </section>
    <section id="form_input_uang_keluar">
        <form class="form-horizontal">
          <h5>Harap Save terlebih dahulu pekerjaan anda sebelum tekan tombol Back</h5>
          <button type="cancel" class="btn btn-danger" id="cancel_input_uang_keluar">Back</button>
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>Uang Keluar</th>
                <th>Keterangan</th>
                <th><button type="submit" class="btn btn-success" id="add_row_input_uang_keluar">Add uang Keluar</button></th>
              </tr>
            </thead>
            <tbody id="table_input_uang_keluar">
              <tr>
                <input type="hidden" id="id_input_uang_keluar" value="">
                <td>
                  <input type="text" id="input_uang_keluar" placeholder="uang keluar" class="form-control">
                </td>
                <td>
                  <input type="text" id="input_uang_keluar_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">                  
                </td>
                <td>
                  <button type="submit" class="btn btn-info" id="submit_input_uang_keluar">Save</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    </section>
    <section id="form_input_pelunasan">
        <form class="form-horizontal">
          <h5>Harap Save terlebih dahulu pekerjaan anda sebelum tekan tombol Back</h5>
          <button type="cancel" class="btn btn-danger" id="cancel_input_pelunasan">Back</button>
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>pelunasan</th>
                <th>Keterangan</th>
                <th><button type="submit" class="btn btn-success" id="add_row_input_pelunasan">Add pelunasan</button></th>
              </tr>
            </thead>
            <tbody id="table_input_pelunasan">
              <tr>
                <input type="hidden" id="id_input_pelunasan" value="">
                <td>
                  <input type="text" id="input_pelunasan" placeholder="pelunasan" class="form-control">
                </td>
                <td>
                  <input type="text" id="input_pelunasan_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">                  
                </td>
                <td>
                  <button type="submit" class="btn btn-info" id="submit_input_pelunasan">Save</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    </section>
    <section id="form_input_operasional">
        <form class="form-horizontal">
          <h5>Harap Save terlebih dahulu pekerjaan anda sebelum tekan tombol Back</h5>
          <button type="cancel" class="btn btn-danger" id="cancel_input_operasional">Back</button>
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>operasional</th>
                <th>Keterangan</th>
                <th><button type="submit" class="btn btn-success" id="add_row_input_operasional">Add operasional</button></th>
              </tr>
            </thead>
            <tbody id="table_input_operasional">
              <tr>
                <input type="hidden" id="id_input_operasional" value="">
                <td>
                  <input type="text" id="input_operasional" placeholder="operasional" class="form-control">
                </td>
                <td>
                  <input type="text" id="input_operasional_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">                  
                </td>
                <td>
                  <button type="submit" class="btn btn-info" id="submit_input_operasional">Save</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    </section>
    <section id="form_input_piutang">
        <form class="form-horizontal">
          <h5>Harap Save terlebih dahulu pekerjaan anda sebelum tekan tombol Back</h5>
          <button type="cancel" class="btn btn-danger" id="cancel_input_piutang">Back</button>
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>piutang</th>
                <th>Keterangan</th>
                <th><button type="submit" class="btn btn-success" id="add_row_input_piutang">Add operasional</button></th>
              </tr>
            </thead>
            <tbody id="table_input_piutang">
              <tr>
                <input type="hidden" id="id_input_piutang" value="">
                <td>
                  <input type="text" id="input_piutang" placeholder="piutang" class="form-control">
                </td>
                <td>
                  <input type="text" id="input_piutang_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">                  
                </td>
                <td>
                  <button type="submit" class="btn btn-info" id="submit_input_piutang">Save</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    </section>
    <section id="form_input_uang_masuk">
        <form class="form-horizontal">
          <h5>Harap Save terlebih dahulu pekerjaan anda sebelum tekan tombol Back</h5>
          <button type="cancel" class="btn btn-danger" id="cancel_input_uang_masuk">Back</button>
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>uang masuk</th>
                <th>Keterangan</th>
                <th><button type="submit" class="btn btn-success" id="add_row_input_uang_masuk">Add uang masuk</button></th>
              </tr>
            </thead>
            <tbody id="table_input_uang_masuk">
              <tr>
                <input type="hidden" id="id_input_uang_masuk" value="">
                <td>
                  <input type="text" id="input_uang_masuk" placeholder="uang masuk" class="form-control">
                </td>
                <td>
                  <input type="text" id="input_uang_masuk_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">                  
                </td>
                <td>
                  <button type="submit" class="btn btn-info" id="submit_input_uang_masuk">Save</button>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    </section>            
  </div>
</div>