<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<table>
	<tr>
		<td><h3>Modal</h3></td>
		<td><h3><?php echo $modal['modal']; ?></h3></td>
	</tr>
	<tr>
		<td><h3>Hybrid</h3></td>
		<td><h3><?php echo $hybrid['hybrid']; ?></h3></td>
	</tr>
	<tr>
		<td><h3>Cash</h3></td>
		<td><h3><?php echo $cash['cash']; ?></h3></td>
	</tr>
	<tr>
		<td><h3>EDC BCA</h3></td>
		<td><h3><?php echo $edc_bca['edc_bca']; ?></h3></td>
	</tr>
	<tr>
		<td><h3>EDC BNI</h3></td>
		<td><h3><?php echo $edc_bni['edc_bni']; ?></h3></td>
	</tr>
</table>
<table>
	<thead>
		<th>No</th>
		<th>Diskon</th>
		<th>Keterangan</th>
	</thead>
	<tbody>
		<?php $no = 0; $td = 0; if($diskon !== FALSE){ foreach ($diskon as $d) { $no++; $td += $d['diskon']; ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $d['diskon'] ?></td>
			<td><?= $d['keterangan'] ?></td>
		</tr>
		<?php }} ?>
	</tbody>
</table>
<table>
	<thead>
		<th>No</th>
		<th>Uang Keluar</th>
		<th>Keterangan</th>
	</thead>
	<tbody>
		<?php $no = 0; $tuk = 0; if($uang_keluar !== FALSE){ foreach ($uang_keluar as $uk) { $no++; $tuk += $uk['uang_keluar']; ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $uk['uang_keluar'] ?></td>
			<td><?= $uk['keterangan'] ?></td>
		</tr>
		<?php }} ?>
	</tbody>
</table>
<table>
	<thead>
		<th>No</th>
		<th>Pelunasan</th>
		<th>Keterangan</th>
	</thead>
	<tbody>
		<?php $no = 0; $tpl = 0; if($pelunasan !== FALSE){ foreach ($pelunasan as $pl) { $no++; $tpl += $pl['pelunasan']; ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $pl['pelunasan'] ?></td>
			<td><?= $pl['keterangan'] ?></td>
		</tr>
		<?php }} ?>
	</tbody>
</table>
<table>
	<thead>
		<th>No</th>
		<th>Operasional</th>
		<th>Keterangan</th>
	</thead>
	<tbody>
		<?php $no = 0; $to = 0; if($operasional !== FALSE){ foreach ($operasional as $o) { $no++; $to += $o['operasional']; ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $o['operasional'] ?></td>
			<td><?= $o['keterangan'] ?></td>
		</tr>
		<?php }} ?>
	</tbody>
</table>
<table>
	<thead>
		<th>No</th>
		<th>Piutang</th>
		<th>Keterangan</th>
	</thead>
	<tbody>
		<?php $no = 0; $tp = 0; if($piutang !== FALSE){ foreach ($piutang as $p) { $no++; $tp += $p['piutang']; ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $p['piutang'] ?></td>
			<td><?= $p['keterangan'] ?></td>
		</tr>
		<?php }} ?>
	</tbody>
</table>
<table>
	<thead>
		<th>No</th>
		<th>Uang Masuk</th>
		<th>Keterangan</th>
	</thead>
	<tbody>
		<?php $no = 0; $tum = 0; if($uang_masuk !== FALSE){ foreach ($uang_masuk as $um) { $no++; $tum += $um['uang_masuk']; ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $um['uang_masuk'] ?></td>
			<td><?= $um['keterangan'] ?></td>
		</tr>
		<?php }} ?>
	</tbody>
</table>
<table>
	<tr>
		<td><h1>Total = </h1></td>
		<td><?php $total = ($cash['cash'] + $edc_bni['edc_bni'] + $edc_bca['edc_bca']) - ($modal['modal'] + $hybrid['hybrid']) + $td +  $tuk - $tpl + $to + $tp - $tum; echo "<h1>".$total."</h1>"; ?></td>
	</tr>
</table>
<script type="text/javascript">
	window.print();
</script>
</body>
</html>