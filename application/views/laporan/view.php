<div class="content-wrapper" id="main">
  <div class="container">
    <section class="content-header">
      <h1>Menu <?php echo ucfirst($this->uri->segment(1)); ?></h1><br>
    </section>
    <section id="loading_user">
      <div class="row">
        <div class="col-lg-7 col-lg-offset-5 col-xs-7 col-xs-offset-5 col-md-7 col-md-offset-5 ">
          <img align="center" alt="loading" src="<?php echo base_url(); ?>assets/img/gear.svg">
        </div>
      </div>
    </section>
    <section id="main_user">
      <div class="row">
        <?php echo form_open('laporan/one'); ?>
        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
          <input type="date" name="date_laporan" class="form-control">
        </div>
        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
          <select class="form-control" name="user_laporan">
            <?php foreach ($user->result_array() as $u) { ?>
              <option value="<?= $u['id_user'] ?>"><?= $u['user'] ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
          <button class="btn btn-success" type="submit">Cetak Laporan Per Karyawan</button>
        </div>
        <?php echo form_close(); ?>
      </div>
    </section>        
  </div>
</div>