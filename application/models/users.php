<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model {

	public $table = 'user';

	public function logged_in(){
		if($this->session->userdata('loged') == TRUE){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function check_user($username,$password){
		$query = $this->db->where($this->table,$username)->get($this->table);
		if($query->num_rows() > 0){
			$data = $query->row_array();
			if($data['password'] !== $password){
				return 'PASSWORD_FALSE';
			} else {
				return $data;
			}
		} else {
			return 'NO_USER';
		}
	}
	function get_all_user(){
		$query = $this->db->get($this->table);
		return $query;
	}
	function get_all_karyawan(){
		$query = $this->db->where('level',2)->get($this->table);
		return $query;
	}
	function insert($data){
		if($this->db->insert($this->table,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function update($id,$data){
		$this->db->where('id_'.$this->table,$id);
		if($this->db->update($this->table,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function get_data_by_id($id){
		return $this->db->where('id_'.$this->table,$id)->get($this->table);
	}
	function delete($id){
		$this->db->where('id_'.$this->table,$id);
		if($this->db->delete($this->table)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

/* End of file users.php */
/* Location: ./application/models/users.php */