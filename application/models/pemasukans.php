<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukans extends CI_Model {

	public $table = 'pemasukan';

	function view_all(){
		$query = $this->db->order_by('id_'.$this->table,'desc')->get($this->table);
		if ($query->num_rows() > 0){
			return $query;
		} else {
			return false;
		}
	}
	function find($id){
		$query = $this->db->where('id_'.$this->table,$id)->get($this->table);
		if ($query->num_rows() > 0){
			return $query;
		} else {
			return false;
		}
	}
	function insert($data){
		$query = $this->db->insert($this->table,$data);
		if ($query){
			return 1;
		} else {
			return false;
		}
	}	
	function update($id,$data){
		$query = $this->db->where('id_'.$this->table,$id)->update($this->table,$data);
		if ($query){
			return 1;
		} else {
			return false;
		}
	}
	function delete($id){
		$query = $this->db->where('id_'.$this->table,$id)->delete($this->table);
		if ($query){
			return 1;
		} else {
			return false;
		}
	}
	function date_counter(){
		$query = $this->db->distinct()
						  ->select('date')
						  ->get($this->table);
		return $query;
	}
	function sum_date($id){
		$query = $this->db->where('date',$id)
						  ->select('jumlah')
						  ->get($this->table);
		return $query;
	}
}

/* End of file pemasukans.php */
/* Location: ./application/models/pemasukans.php */