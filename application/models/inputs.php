<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inputs extends CI_Model {

	public $modal = 'modal';
	public $hybrid = 'hybrid';
	public $cash = 'cash';
	public $edc_bca = 'edc_bca';
	public $edc_bni = 'edc_bni';
	public $diskon = 'diskon';
	public $uang_keluar = 'uang_keluar';
	public $pelunasan = 'pelunasan';
	public $operasional = 'operasional';
	public $piutang = 'piutang';
	public $uang_masuk = 'uang_masuk';

	function modal_insert($data){
		if($this->db->insert($this->modal,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function modal_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->modal);
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}
	function modal_update($id,$data){
		$this->db->where('id_'.$this->modal,$id);
		if($this->db->update($this->modal,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//hybrid segment
	function hybrid_insert($data){
		if($this->db->insert($this->hybrid,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function hybrid_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->hybrid);
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}
	function hybrid_update($id,$data){
		$this->db->where('id_'.$this->hybrid,$id);
		if($this->db->update($this->hybrid,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//cash segment
	function cash_insert($data){
		if($this->db->insert($this->cash,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function cash_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->cash);
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}
	function cash_update($id,$data){
		$this->db->where('id_'.$this->cash,$id);
		if($this->db->update($this->cash,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//edc_bca segment
	function edc_bca_insert($data){
		if($this->db->insert($this->edc_bca,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function edc_bca_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->edc_bca);
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}
	function edc_bca_update($id,$data){
		$this->db->where('id_'.$this->edc_bca,$id);
		if($this->db->update($this->edc_bca,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//edc_bni segment
	function edc_bni_insert($data){
		if($this->db->insert($this->edc_bni,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function edc_bni_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->edc_bni);
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}
	function edc_bni_update($id,$data){
		$this->db->where('id_'.$this->edc_bni,$id);
		if($this->db->update($this->edc_bni,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//diskon segment
	function diskon_insert($data){
		if($this->db->insert($this->diskon,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function diskon_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->diskon);
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}
	function diskon_update($id,$data){
		$this->db->where('id_'.$this->diskon,$id);
		if($this->db->update($this->diskon,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//uang_keluar segment
	function uang_keluar_insert($data){
		if($this->db->insert($this->uang_keluar,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function uang_keluar_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->uang_keluar);
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}
	function uang_keluar_update($id,$data){
		$this->db->where('id_'.$this->uang_keluar,$id);
		if($this->db->update($this->uang_keluar,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//pelunasan segment
	function pelunasan_insert($data){
		if($this->db->insert($this->pelunasan,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function pelunasan_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->pelunasan);
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}
	function pelunasan_update($id,$data){
		$this->db->where('id_'.$this->pelunasan,$id);
		if($this->db->update($this->pelunasan,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//operasional segment
	function operasional_insert($data){
		if($this->db->insert($this->operasional,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function operasional_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->operasional);
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}
	function operasional_update($id,$data){
		$this->db->where('id_'.$this->operasional,$id);
		if($this->db->update($this->operasional,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//piutang segment
	function piutang_insert($data){
		if($this->db->insert($this->piutang,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function piutang_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->piutang);
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}
	function piutang_update($id,$data){
		$this->db->where('id_'.$this->piutang,$id);
		if($this->db->update($this->piutang,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	//uang_masuk segment
	function uang_masuk_insert($data){
		if($this->db->insert($this->uang_masuk,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function uang_masuk_check($user,$date){
		$query = $this->db->where('id_users',$user)->where('date',$date)->get($this->uang_masuk);
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}
	function uang_masuk_update($id,$data){
		$this->db->where('id_'.$this->uang_masuk,$id);
		if($this->db->update($this->uang_masuk,$data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

/* End of file inputs.php */
/* Location: ./application/models/inputs.php */