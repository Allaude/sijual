jQuery(document).ready(function($) {
	$('#loader').hide();
	$('#cari').click(function(event) {
		var id = $('#search').val();
		var method = 'find_by_pelanggan/'+id;
		super_ajax(method,function(data){
			$('#loader').show();
			$.each(data, function(index, val) {
				$('#view_permintaan').empty();
				$('#view_permintaan').append('<tr><td>'+val.provider+' '+val.nama_product+'</td><td>'+val.h_beli+'</td><td>'+val.jenis_pelanggan+' '+val.nama+'</td><td>'+val.qty_permintaan+'</td><td>'+val.date_permintaan+'</td><td>'+val.stock_transaksi+'</td><td><a href="http://localhost/pulsa/index.php/permintaan/delete/'+val.id_permintaan+'" class="btn btn-danger">X</a></td></tr>');
				$('#loader').hide();
			});
		});
	});
	function super_ajax(method,callback){
		$.ajax({
			url: 'permintaan/'+method,
			type: 'GET',
			dataType: 'json',
		})
		.done(function(data) {
			callback(data);
		})
		.fail(function(err) {
			console.log(err);
		})
		.always(function() {
			console.log("complete");
		});
		
	}
});