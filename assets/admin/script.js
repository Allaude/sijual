jQuery(document).ready(function($) {
	//default
	var base_url = 'http://localhost/sijual/index.php/';
	$('#loading_user').hide();
	$('#form_user').hide();
	$('#loading_input').hide();
	$('#form_input_modal').hide();
	$('#form_input_hybrid').hide();
	$('#form_input_cash').hide();
	$('#form_input_edc_bca').hide();
	$('#form_input_edc_bni').hide();
	$('#form_input_diskon').hide();
	$('#form_input_uang_keluar').hide();
	$('#form_input_pelunasan').hide();
	$('#form_input_operasional').hide();
	$('#form_input_piutang').hide();
	$('#form_input_uang_masuk').hide();
	//function
	function set_ajax(url,method,type,data,callback){
		$.ajax({
			url: base_url+url,
			type: method,
			dataType: type,
			data: data,
		})
		.done(function(r) {
			callback(r);
		})
		.fail(function(e) {
			console.log(e);
		})
		.always(function() {
			console.log("complete");
		});
	}
	//user segment
	$(document).on('click', '#update_user', function(event) {
		event.preventDefault();
		var id = $(this).val();
		$('#loading_user').show();
		set_ajax('user/get_detail_user/'+id,'GET','json','',function(data){
				$('#id_user').val(data.id_user);
				$('#inputUser').val(data.user);
				$('#inputPassword').val(data.password);
				$('#inputLevel').val(data.level);
				$('#loading_user').hide();
				$('#form_user').show(300);
				$('#main_user').hide(300);
		});
	});
	$(document).on('click', '#add_user', function(event) {
		event.preventDefault();
		$('#form_user').show(300);
		$('#main_user').hide(300);
	});
	$(document).on('click', '#cancel_input_user', function(event) {
		event.preventDefault();
		$('#form_user').hide(300);
		$('#main_user').show(300);		
	});
	$(document).on('click', '#submit_user', function(event) {
		event.preventDefault();
		var id = $('#id_user').val();
		var username = $('#inputUser').val();
		var password = $('#inputPassword').val();
		var level = $('#inputLevel').val();
		if(username == '' || password == ''){
			alert("Check Your input it must not empty");
		} else {
			$('#form_user').hide(300);
			$('#loading_user').show();
			var data = {username : username,password:password,level:level};
			if(id == ''){
				var url = 'user/input';
			} else {
				var url = 'user/update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_user').hide();
					window.location = base_url+'user';
				}
			});
		}
	});
	$(document).on('click', '#delete_user', function(event) {
		event.preventDefault();
		var id = $(this).val();
		var message = confirm("Apakah Anda Yakin Hapus Data Ini? ");
		if(message == true){
			$('#loading_user').show();
			set_ajax('user/delete/'+id,'POST','json','',function(data){
				if(data == true){
					$('#loading_user').hide();
					window.location = base_url+'user';					
				}
			});
		}
	});
	//input segment
	//sub sector modal
	$(document).on('click', '#add_input_modal', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#main_user').hide(300);
		set_ajax('input/modal_check','GET','json','',function(data){
			if(data !== false){
				$('#id_input_modal').val(data.id_modal);
				$('#input_modal').val(data.modal);
				$('#input_modal_keterangan').val(data.keterangan);	
			}
				$('#form_input_modal').show(300);
				$('#loading_input').hide();
		});
	});
	$(document).on('click', '#cancel_input_modal', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_modal').hide(300);
	});
	$(document).on('click', '#submit_input_modal', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		var id = $('#id_input_modal').val();
		var modal = $('#input_modal').val();
		var ket = $('#input_modal_keterangan').val();
		var data = {modal : modal,ket:ket};
		if(modal == ''){
			alert('check your input modal can not empty');
		}
		if(id == ''){
			var url = 'input/modal_insert';
		} else {
			var url = 'input/modal_update/'+id;
		}
		set_ajax(url,'POST','json',data,function(data){
			if(data == true){
				$('#loading_input').hide();
				window.location = base_url+'input';					
			}			
		});
	});
	//sub sector hybrid
	$(document).on('click', '#add_input_hybrid', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#main_user').hide(300);
		set_ajax('input/hybrid_check','GET','json','',function(data){
			if(data !== false){
				$('#id_input_hybrid').val(data.id_hybrid);
				$('#input_hybrid').val(data.hybrid);
				$('#input_hybrid_keterangan').val(data.keterangan);	
			}
			$('#form_input_hybrid').show(300);
			$('#loading_input').hide();
		});
	});
	$(document).on('click', '#cancel_input_hybrid', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_hybrid').hide(300);
	});
	$(document).on('click', '#submit_input_hybrid', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		var id = $('#id_input_hybrid').val();
		var hybrid = $('#input_hybrid').val();
		var ket = $('#input_hybrid_keterangan').val();
		var data = {hybrid : hybrid,ket:ket};
		if(hybrid == ''){
			alert('check your input hybrid can not empty');
		}
		if(id == ''){
			var url = 'input/hybrid_insert';
		} else {
			var url = 'input/hybrid_update/'+id;
		}
		set_ajax(url,'POST','json',data,function(data){
			if(data == true){
				$('#loading_input').hide();
				window.location = base_url+'input';					
			}			
		});
	});
	//sub sector cash
	$(document).on('click', '#add_input_cash', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#main_user').hide(300);
		set_ajax('input/cash_check','GET','json','',function(data){
			if(data !== false){
				$('#id_input_cash').val(data.id_cash);
				$('#input_cash').val(data.cash);
				$('#input_cash_keterangan').val(data.keterangan);	
			}
			$('#form_input_cash').show(300);
			$('#loading_input').hide();
		});
	});
	$(document).on('click', '#cancel_input_cash', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_cash').hide(300);
	});
	$(document).on('click', '#submit_input_cash', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		var id = $('#id_input_cash').val();
		var cash = $('#input_cash').val();
		var ket = $('#input_cash_keterangan').val();
		var data = {cash : cash,ket:ket};
		if(cash == ''){
			alert('check your input cash can not empty');
		}
		if(id == ''){
			var url = 'input/cash_insert';
		} else {
			var url = 'input/cash_update/'+id;
		}
		set_ajax(url,'POST','json',data,function(data){
			if(data == true){
				$('#loading_input').hide();
				window.location = base_url+'input';					
			}			
		});
	});
	//sub sector edc_bca
	$(document).on('click', '#add_input_edc_bca', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#main_user').hide(300);
		set_ajax('input/edc_bca_check','GET','json','',function(data){
			if(data !== false){
				$('#id_input_edc_bca').val(data.id_edc_bca);
				$('#input_edc_bca').val(data.edc_bca);
				$('#input_edc_bca_keterangan').val(data.keterangan);	
			}
			$('#form_input_edc_bca').show(300);
			$('#loading_input').hide();
		});
	});
	$(document).on('click', '#cancel_input_edc_bca', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_edc_bca').hide(300);
	});
	$(document).on('click', '#submit_input_edc_bca', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		var id = $('#id_input_edc_bca').val();
		var edc_bca = $('#input_edc_bca').val();
		var ket = $('#input_edc_bca_keterangan').val();
		var data = {edc_bca : edc_bca,ket:ket};
		if(edc_bca == ''){
			alert('check your input edc bca can not empty');
		}
		if(id == ''){
			var url = 'input/edc_bca_insert';
		} else {
			var url = 'input/edc_bca_update/'+id;
		}
		set_ajax(url,'POST','json',data,function(data){
			if(data == true){
				$('#loading_input').hide();
				window.location = base_url+'input';					
			}			
		});
	});
	//sub sector edc_bni
	$(document).on('click', '#add_input_edc_bni', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#main_user').hide(300);
		set_ajax('input/edc_bni_check','GET','json','',function(data){
			if(data !== false){
				$('#id_input_edc_bni').val(data.id_edc_bni);
				$('#input_edc_bni').val(data.edc_bni);
				$('#input_edc_bni_keterangan').val(data.keterangan);	
			}
			$('#form_input_edc_bni').show(300);
			$('#loading_input').hide();
		});
	});
	$(document).on('click', '#cancel_input_edc_bni', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_edc_bni').hide(300);
	});
	$(document).on('click', '#submit_input_edc_bni', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		var id = $('#id_input_edc_bni').val();
		var edc_bni = $('#input_edc_bni').val();
		var ket = $('#input_edc_bni_keterangan').val();
		var data = {edc_bni : edc_bni,ket:ket};
		if(edc_bni == ''){
			alert('check your input edc bni can not empty');
		}
		if(id == ''){
			var url = 'input/edc_bni_insert';
		} else {
			var url = 'input/edc_bni_update/'+id;
		}
		set_ajax(url,'POST','json',data,function(data){
			if(data == true){
				$('#loading_input').hide();
				window.location = base_url+'input';					
			}			
		});
	});
	//diskon segment
	$(document).on('click', '#add_input_diskon', function(event) {
		event.preventDefault();
		set_ajax('input/diskon_check','GET','json','',function(data){
			if(data !== false){
			$('#table_input_diskon').empty();
			$.each(data, function(index, val) {
				$('#table_input_diskon').append('<tr>'+
		                '<input type="hidden" id="id_input_diskon" value="'+val.id_diskon+'">'+
		                '<td>'+
		                  '<input type="text" id="input_diskon" placeholder="diskon" class="form-control" value="'+val.diskon+'" >'+
		                '</td>'+
		                '<td>'+
		                  '<input type="text" id="input_diskon_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control" value="'+val.keterangan+'">'+                  
		                '</td>'+
		                '<td>'+
		                  '<button type="submit" class="btn btn-info" id="submit_input_diskon">Save</button>'+
		                '</td>'+
		              '</tr>');
			});				
			}
		});
		$('#main_user').hide(300);
		$('#form_input_diskon').show(300);
	});
	$(document).on('click', '#cancel_input_diskon', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_diskon').hide(300);
	});
	$(document).on('click', '#add_row_input_diskon', function(event) {
		event.preventDefault();
		$('#table_input_diskon').append('<tr>'+
                '<input type="hidden" id="id_input_diskon" value="">'+
                '<td>'+
                  '<input type="text" id="input_diskon" placeholder="diskon" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<input type="text" id="input_diskon_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">'+                  
                '</td>'+
                '<td>'+
                  '<button type="submit" class="btn btn-info" id="submit_input_diskon">Save</button>'+
                '</td>'+
              '</tr>');
	});
	$(document).on('click', '#submit_input_diskon', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#form_input_diskon').hide();
		$(this).parents('tr').each(function(index, el) {
			var id = $(el).find('#id_input_diskon').val();
			var diskon = $(el).find('#input_diskon').val();
			var keterangan = $(el).find('#input_diskon_keterangan').val();
			var data = {diskon:diskon,keterangan:keterangan};
			if(id == ''){
				var url = 'input/diskon_insert'
			} else {
				var url = 'input/diskon_update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_input').hide();
					$('#form_input_diskon').show();
					alert("Data Telah Tersimpan");					
				}				
			});
		});
	});
	//uang_keluar segment
	$(document).on('click', '#add_input_uang_keluar', function(event) {
		event.preventDefault();
		set_ajax('input/uang_keluar_check','GET','json','',function(data){
			if(data !== false){
			$('#table_input_uang_keluar').empty();
			$.each(data, function(index, val) {
				$('#table_input_uang_keluar').append('<tr>'+
		                '<input type="hidden" id="id_input_uang_keluar" value="'+val.id_uang_keluar+'">'+
		                '<td>'+
		                  '<input type="text" id="input_uang_keluar" placeholder="uang keluar" class="form-control" value="'+val.uang_keluar+'" >'+
		                '</td>'+
		                '<td>'+
		                  '<input type="text" id="input_uang_keluar_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control" value="'+val.keterangan+'">'+                  
		                '</td>'+
		                '<td>'+
		                  '<button type="submit" class="btn btn-info" id="submit_input_uang_keluar">Save</button>'+
		                '</td>'+
		              '</tr>');
			});				
			}
		});
		$('#main_user').hide(300);
		$('#form_input_uang_keluar').show(300);
	});
	$(document).on('click', '#cancel_input_uang_keluar', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_uang_keluar').hide(300);
	});
	$(document).on('click', '#add_row_input_uang_keluar', function(event) {
		event.preventDefault();
		$('#table_input_uang_keluar').append('<tr>'+
                '<input type="hidden" id="id_input_uang_keluar" value="">'+
                '<td>'+
                  '<input type="text" id="input_uang_keluar" placeholder="uang keluar" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<input type="text" id="input_uang_keluar_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">'+                  
                '</td>'+
                '<td>'+
                  '<button type="submit" class="btn btn-info" id="submit_input_uang_keluar">Save</button>'+
                '</td>'+
              '</tr>');
	});
	$(document).on('click', '#submit_input_uang_keluar', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#form_input_uang_keluar').hide();
		$(this).parents('tr').each(function(index, el) {
			var id = $(el).find('#id_input_uang_keluar').val();
			var uang_keluar = $(el).find('#input_uang_keluar').val();
			var keterangan = $(el).find('#input_uang_keluar_keterangan').val();
			var data = {uang_keluar:uang_keluar,keterangan:keterangan};
			if(id == ''){
				var url = 'input/uang_keluar_insert'
			} else {
				var url = 'input/uang_keluar_update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_input').hide();
					$('#form_input_uang_keluar').show();
					alert("Data Telah Tersimpan");					
				}				
			});
		});
	});
	//pelunasan segment
	$(document).on('click', '#add_input_pelunasan', function(event) {
		event.preventDefault();
		set_ajax('input/pelunasan_check','GET','json','',function(data){
			console.log(data);
			if(data !== false){
			$('#table_input_pelunasan').empty();
			$.each(data, function(index, val) {
				console.log(val);
				$('#table_input_pelunasan').append('<tr>'+
		                '<input type="hidden" id="id_input_pelunasan" value="'+val.id_pelunasan+'">'+
		                '<td>'+
		                  '<input type="text" id="input_pelunasan" placeholder="pelunasan" class="form-control" value="'+val.pelunasan+'" >'+
		                '</td>'+
		                '<td>'+
		                  '<input type="text" id="input_pelunasan_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control" value="'+val.keterangan+'">'+                  
		                '</td>'+
		                '<td>'+
		                  '<button type="submit" class="btn btn-info" id="submit_input_pelunasan">Save</button>'+
		                '</td>'+
		              '</tr>');
			});				
			}
		});
		$('#main_user').hide(300);
		$('#form_input_pelunasan').show(300);
	});
	$(document).on('click', '#cancel_input_pelunasan', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_pelunasan').hide(300);
	});
	$(document).on('click', '#add_row_input_pelunasan', function(event) {
		event.preventDefault();
		$('#table_input_pelunasan').append('<tr>'+
                '<input type="hidden" id="id_input_pelunasan" value="">'+
                '<td>'+
                  '<input type="text" id="input_pelunasan" placeholder="pelunasan" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<input type="text" id="input_pelunasan_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">'+                  
                '</td>'+
                '<td>'+
                  '<button type="submit" class="btn btn-info" id="submit_input_pelunasan">Save</button>'+
                '</td>'+
              '</tr>');
	});
	$(document).on('click', '#submit_input_pelunasan', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#form_input_pelunasan').hide();
		$(this).parents('tr').each(function(index, el) {
			var id = $(el).find('#id_input_pelunasan').val();
			var pelunasan = $(el).find('#input_pelunasan').val();
			var keterangan = $(el).find('#input_pelunasan_keterangan').val();
			var data = {pelunasan:pelunasan,keterangan:keterangan};
			if(id == ''){
				var url = 'input/pelunasan_insert'
			} else {
				var url = 'input/pelunasan_update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_input').hide();
					$('#form_input_pelunasan').show();
					alert("Data Telah Tersimpan");					
				}				
			});
		});
	});
	//operasional segment
	$(document).on('click', '#add_input_operasional', function(event) {
		event.preventDefault();
		set_ajax('input/operasional_check','GET','json','',function(data){
			console.log(data);
			if(data !== false){
			$('#table_input_operasional').empty();
			$.each(data, function(index, val) {
				console.log(val);
				$('#table_input_operasional').append('<tr>'+
		                '<input type="hidden" id="id_input_operasional" value="'+val.id_operasional+'">'+
		                '<td>'+
		                  '<input type="text" id="input_operasional" placeholder="pelunasan" class="form-control" value="'+val.operasional+'" >'+
		                '</td>'+
		                '<td>'+
		                  '<input type="text" id="input_operasional_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control" value="'+val.keterangan+'">'+                  
		                '</td>'+
		                '<td>'+
		                  '<button type="submit" class="btn btn-info" id="submit_input_operasional">Save</button>'+
		                '</td>'+
		              '</tr>');
			});				
			}
		});
		$('#main_user').hide(300);
		$('#form_input_operasional').show(300);
	});
	$(document).on('click', '#cancel_input_operasional', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_operasional').hide(300);
	});
	$(document).on('click', '#add_row_input_operasional', function(event) {
		event.preventDefault();
		$('#table_input_operasional').append('<tr>'+
                '<input type="hidden" id="id_input_operasional" value="">'+
                '<td>'+
                  '<input type="text" id="input_operasional" placeholder="operasional" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<input type="text" id="input_operasional_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">'+                  
                '</td>'+
                '<td>'+
                  '<button type="submit" class="btn btn-info" id="submit_input_operasional">Save</button>'+
                '</td>'+
              '</tr>');
	});
	$(document).on('click', '#submit_input_operasional', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#form_input_operasional').hide();
		$(this).parents('tr').each(function(index, el) {
			var id = $(el).find('#id_input_operasional').val();
			var operasional = $(el).find('#input_operasional').val();
			var keterangan = $(el).find('#input_operasional_keterangan').val();
			var data = {operasional:operasional,keterangan:keterangan};
			if(id == ''){
				var url = 'input/operasional_insert'
			} else {
				var url = 'input/operasional_update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_input').hide();
					$('#form_input_operasional').show();
					alert("Data Telah Tersimpan");					
				}				
			});
		});
	});
	//piutang segment
	$(document).on('click', '#add_input_piutang', function(event) {
		event.preventDefault();
		set_ajax('input/piutang_check','GET','json','',function(data){
			console.log(data);
			if(data !== false){
			$('#table_input_piutang').empty();
			$.each(data, function(index, val) {
				$('#table_input_piutang').append('<tr>'+
		                '<input type="hidden" id="id_input_piutang" value="'+val.id_piutang+'">'+
		                '<td>'+
		                  '<input type="text" id="input_piutang" placeholder="piutang" class="form-control" value="'+val.piutang+'" >'+
		                '</td>'+
		                '<td>'+
		                  '<input type="text" id="input_piutang_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control" value="'+val.keterangan+'">'+                  
		                '</td>'+
		                '<td>'+
		                  '<button type="submit" class="btn btn-info" id="submit_input_piutang">Save</button>'+
		                '</td>'+
		              '</tr>');
			});				
			}
		});
		$('#main_user').hide(300);
		$('#form_input_piutang').show(300);
	});
	$(document).on('click', '#cancel_input_piutang', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_piutang').hide(300);
	});
	$(document).on('click', '#add_row_input_piutang', function(event) {
		event.preventDefault();
		$('#table_input_piutang').append('<tr>'+
                '<input type="hidden" id="id_input_piutang" value="">'+
                '<td>'+
                  '<input type="text" id="input_piutang" placeholder="piutang" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<input type="text" id="input_piutang_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">'+                  
                '</td>'+
                '<td>'+
                  '<button type="submit" class="btn btn-info" id="submit_input_piutang">Save</button>'+
                '</td>'+
              '</tr>');
	});
	$(document).on('click', '#submit_input_piutang', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#form_input_piutang').hide();
		$(this).parents('tr').each(function(index, el) {
			var id = $(el).find('#id_input_piutang').val();
			var piutang = $(el).find('#input_piutang').val();
			var keterangan = $(el).find('#input_piutang_keterangan').val();
			var data = {piutang:piutang,keterangan:keterangan};
			if(id == ''){
				var url = 'input/piutang_insert'
			} else {
				var url = 'input/piutang_update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_input').hide();
					$('#form_input_piutang').show();
					alert("Data Telah Tersimpan");					
				}				
			});
		});
	});
	//uang_masuk segment
	$(document).on('click', '#add_input_uang_masuk', function(event) {
		event.preventDefault();
		set_ajax('input/uang_masuk_check','GET','json','',function(data){
			console.log(data);
			if(data !== false){
			$('#table_input_uang_masuk').empty();
			$.each(data, function(index, val) {
				$('#table_input_uang_masuk').append('<tr>'+
		                '<input type="hidden" id="id_input_uang_masuk" value="'+val.id_uang_masuk+'">'+
		                '<td>'+
		                  '<input type="text" id="input_uang_masuk" placeholder="uang masuk" class="form-control" value="'+val.uang_masuk+'" >'+
		                '</td>'+
		                '<td>'+
		                  '<input type="text" id="input_uang_masuk_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control" value="'+val.keterangan+'">'+                  
		                '</td>'+
		                '<td>'+
		                  '<button type="submit" class="btn btn-info" id="submit_input_uang_masuk">Save</button>'+
		                '</td>'+
		              '</tr>');
			});				
			}
		});
		$('#main_user').hide(300);
		$('#form_input_uang_masuk').show(300);
	});
	$(document).on('click', '#cancel_input_uang_masuk', function(event) {
		event.preventDefault();
		$('#main_user').show(300);
		$('#form_input_uang_masuk').hide(300);
	});
	$(document).on('click', '#add_row_input_uang_masuk', function(event) {
		event.preventDefault();
		$('#table_input_uang_masuk').append('<tr>'+
                '<input type="hidden" id="id_input_uang_masuk" value="">'+
                '<td>'+
                  '<input type="text" id="input_uang_masuk" placeholder="uang masuk" class="form-control">'+
                '</td>'+
                '<td>'+
                  '<input type="text" id="input_uang_masuk_keterangan" placeholder="Isi Keterangan Jika Ada" class="form-control">'+                  
                '</td>'+
                '<td>'+
                  '<button type="submit" class="btn btn-info" id="submit_input_uang_masuk">Save</button>'+
                '</td>'+
              '</tr>');
	});
	$(document).on('click', '#submit_input_uang_masuk', function(event) {
		event.preventDefault();
		$('#loading_input').show();
		$('#form_input_uang_masuk').hide();
		$(this).parents('tr').each(function(index, el) {
			var id = $(el).find('#id_input_uang_masuk').val();
			var uang_masuk = $(el).find('#input_uang_masuk').val();
			var keterangan = $(el).find('#input_uang_masuk_keterangan').val();
			var data = {uang_masuk:uang_masuk,keterangan:keterangan};
			if(id == ''){
				var url = 'input/uang_masuk_insert'
			} else {
				var url = 'input/uang_masuk_update/'+id;
			}
			set_ajax(url,'POST','json',data,function(data){
				if(data == true){
					$('#loading_input').hide();
					$('#form_input_uang_masuk').show();
					alert("Data Telah Tersimpan");					
				}				
			});
		});
	});
});